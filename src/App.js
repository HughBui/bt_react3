import logo from "./logo.svg";
import "./App.css";
import BT_PhoneShop from "./BT_PhoneShop/BT_PhoneShop";

function App() {
  return (
    <div className="App">
      <BT_PhoneShop />
    </div>
  );
}

export default App;
