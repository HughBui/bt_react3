import React, { Component } from "react";
import ItemPhone from "./ItemPhone";

export default class ListPhone extends Component {
  render() {
    return (
      <div>
        <div className="row">
          {this.props.dataListPhone.map((item) => {
            return (
              <ItemPhone
                key={item.maSP}
                dataProduct={item}
                handleSP={this.props.handleListPhone}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
