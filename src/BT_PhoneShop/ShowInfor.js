import React, { Component } from "react";

export default class ShowInfor extends Component {
  render() {
    let {
      maSP,
      tenSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      giaBan,
      hinhAnh,
    } = this.props.showInfor;
    console.log("test1", this.props.visibility);
    return (
      <div className="d-flex mt-5">
        <div>
          <h4>{tenSP}</h4>
          <img style={{ height: "550px" }} src={hinhAnh} alt="" />{" "}
        </div>
        <div class="container" style={{ visibility: this.props.visibility }}>
          <h2 className="mb-5">Thông Số Kỹ Thuật</h2>
          <table class="table">
            <tbody>
              <tr>
                <td className="font-weight-bold">Màn hình</td>
                <td>{manHinh}</td>
              </tr>
              <tr>
                <td className="font-weight-bold">Hệ điều hành</td>
                <td>{heDieuHanh}</td>
              </tr>
              <tr>
                <td className="font-weight-bold">Camera trước</td>
                <td>{cameraTruoc}</td>
              </tr>
              <tr>
                <td className="font-weight-bold">Camera sau</td>
                <td>{cameraSau}</td>
              </tr>
              <tr>
                <td className="font-weight-bold">Ram</td>
                <td>{ram}</td>
              </tr>
              <tr>
                <td className="font-weight-bold">Rom</td>
                <td>{rom}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
// maSP: 2,
// tenSP: "Meizu 16Xs",
// manHinh: "AMOLED, FHD+ 2232 x 1080 pixels",
// heDieuHanh: "Android 9.0 (Pie); Flyme",
// cameraTruoc: "20 MP",
// cameraSau: "Chính 48 MP & Phụ 8 MP, 5 MP",
// ram: "4 GB",
// rom: "64 GB",
// giaBan: 7600000,
// hinhAnh: "./img/meizuphone.jpg",
