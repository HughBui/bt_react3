import React, { Component } from "react";
import { data } from "./dataPhone";
import ListPhone from "./ListPhone";
import ShowInfor from "./ShowInfor";

export default class BT_PhoneShop extends Component {
  state = {
    dataPhone: data,
    itemShow: [],
    infor: [],
    visibility: "hidden",
  };
  handleLayThongTinSp = (sanPham) => {
    let cloneDataPhone = [...data];
    console.log("yes");

    let index = cloneDataPhone.findIndex((item) => {
      return item.maSP == sanPham;
    });
    this.setState({ visibility: "" });
    console.log("index", index);
    let itemShow = cloneDataPhone[index];
    console.log(itemShow);
    this.setState({
      infor: {
        tenSP: itemShow.tenSP,
        manHinh: itemShow.manHinh,
        heDieuHanh: itemShow.heDieuHanh,
        cameraTruoc: itemShow.cameraTruoc,
        cameraSau: itemShow.cameraSau,
        ram: itemShow.ram,
        rom: itemShow.rom,
        hinhAnh: itemShow.hinhAnh,
      },
    });
  };

  render() {
    console.log("infor1", this.state.infor);
    return (
      <div>
        <ListPhone
          dataListPhone={this.state.dataPhone}
          handleListPhone={this.handleLayThongTinSp}
        />
        <ShowInfor
          showInfor={this.state.infor}
          visibility={this.state.visibility}
        />
      </div>
    );
  }
}
