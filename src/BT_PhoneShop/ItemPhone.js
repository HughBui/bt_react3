import React, { Component } from "react";
import ShowInfor from "./ShowInfor";

export default class ItemPhone extends Component {
  render() {
    let { tenSP, hinhAnh, giaBan, maSP } = this.props.dataProduct;
    console.log("maSP", maSP);
    return (
      <div className="col-4 ">
        <img style={{ height: "400px" }} src={hinhAnh}></img>
        <p className="display-4">{tenSP}</p>
        <h5 className="text-warning font-weight-bold">
          Giá sốc : {giaBan} VND
        </h5>
        <div>
          <button
            className="btn btn-success mt-3"
            onClick={() => {
              this.props.handleSP(maSP);
            }}
          >
            Xem Chi Tiết
          </button>
        </div>
        {/* <ShowInfor /> */}
      </div>
    );
  }
}
